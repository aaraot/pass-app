import {Component, OnDestroy, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {Subscription} from 'rxjs';

import {AlertService} from '../_services';

@Component({
  selector: 'app-alert',
  templateUrl: 'alert.component.html',
  styleUrls: ['./alert.component.css'],
  animations: [
    trigger('show', [
      transition(':enter', [
        style({transform: 'translateY(150%)'}),
        animate('350ms ease-in-out', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('350ms ease-in-out', style({transform: 'translateY(150%)'}))
      ])
    ])
  ]
})
export class AlertComponent implements OnInit, OnDestroy {
  message: any;
  animate = false;
  private subscription: Subscription;

  constructor(private alertService: AlertService) {
  }

  ngOnInit() {
    this.subscription = this.alertService.getMessage().subscribe(message => {
      this.message = message;

      if (this.message !== undefined) {
        this.animate = true;

        setTimeout(function () {
          // clear alert
          this.toggle();
        }.bind(this), this.message.timeout);
      }
    });
  }

  toggle() {
    this.message = null;
    this.animate = false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
