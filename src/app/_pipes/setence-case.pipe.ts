import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'sentenceCase'})
export class SentenceCasePipe implements PipeTransform {
  transform(camelCase: string): string {
    return camelCase
      // insert a space before all caps
      .replace(/([A-Z])/g, ' $1')
      // uppercase the first character
      .replace(/^./, function (str) {
        return str.toUpperCase();
      });
  }
}
