import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'snakeCase'})
export class SnakeCasePipe implements PipeTransform {
  transform(camelCase: string): string {
    return camelCase
      // insert a space before all caps
      .replace(/_/g, ' ')
      // uppercase the first character
      .replace(/^./, function (str) {
        return str.toUpperCase();
      });
  }
}
