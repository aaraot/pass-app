import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';

import {Password, User} from '../_models';
import {PasswordService} from '../_services';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pageTitle = 'My Passwords';
  currentUser: User;
  passwords: Password[] = [];
  sharedPasswords: Password[] = [];

  // Tab
  viewMode = 'tab1';

  // Datatable
  columns: Array<any>;

  constructor(private userService: PasswordService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.columns = [
      'select',
      'project_name',
      'project_link',
      'description',
      'username',
      'password',
      'actions'
    ];
  }

  ngOnInit() {
    this.loadAllPasswords();
  }

  private loadAllPasswords() {
    this.userService.getAll().pipe(first()).subscribe(passwords => {
      this.passwords = passwords;
    });
    this.userService.getAllShared().pipe(first()).subscribe(passwords => {
      this.sharedPasswords = passwords;
    });
  }
}
