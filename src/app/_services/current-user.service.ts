import {Injectable} from '@angular/core';

@Injectable()
export class CurrentUserService {
  constructor() {
  }

  getUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getToken() {
    return JSON.parse(localStorage.getItem('token'));
  }
}
