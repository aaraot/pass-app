import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Password} from '../_models';
import {CurrentUserService} from './current-user.service';

@Injectable()
export class PasswordService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.getToken().access_token
    })
  };

  constructor(private currentUser: CurrentUserService, private http: HttpClient) {
  }

  getAll() {
    return this.http.get<Password[]>(`${environment.apiUrl}/passwords`, this.httpOptions);
  }

  getAllShared() {
    return this.http.get<Password[]>(`${environment.apiUrl}/shared-passwords`, this.httpOptions);
  }

  getById(id: number) {
    return this.http.get(`${environment.apiUrl}/passwords/` + id, this.httpOptions);
  }

  store(password: Password) {

    return this.http.post(`${environment.apiUrl}/passwords/store`, {
      password: password,
      user: this.currentUser.getUser().client_id
    }, this.httpOptions);
  }

  update(id: number, password: Password) {
    return this.http.put(`${environment.apiUrl}/passwords/update/` + id, password, this.httpOptions);
  }

  destroy(id: number) {
    return this.http.delete(`${environment.apiUrl}/passwords/destroy/` + id, this.httpOptions);
  }

  generate(length, useUpper, useNumbers, userSymbols) {
    const passwordLength = length || 12;
    const addUpper = useUpper || true;
    const addNumbers = useNumbers || true;
    const addSymbols = userSymbols || true;

    const lowerCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    const upperCharacters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const symbols = ['!', '?', '@'];

    const getRandom = function (array) {
      return array[Math.floor(Math.random() * array.length)];
    };

    let finalCharacters = '';

    if (addUpper) {
      finalCharacters = finalCharacters.concat(getRandom(upperCharacters));
    }

    if (addNumbers) {
      finalCharacters = finalCharacters.concat(getRandom(numbers));
    }

    if (addSymbols) {
      finalCharacters = finalCharacters.concat(getRandom(symbols));
    }

    for (let i = 1; i < passwordLength - 3; i++) {
      finalCharacters = finalCharacters.concat(getRandom(lowerCharacters));
    }

    return finalCharacters.split('').sort(function () {
      return 0.5 - Math.random();
    }).join('');
  }
}
