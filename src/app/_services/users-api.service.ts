import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CurrentUserService} from './current-user.service';

@Injectable()
export class UserAPIService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.getToken().access_token
    })
  };

  constructor(private currentUser: CurrentUserService, private http: HttpClient) {
  }

  getAll(id: number) {
    return this.http.get<any>(`${environment.apiUrl}/users-api/` + id, this.httpOptions);
  }

  sharePassword(id: number, ids: any) {
    return this.http.put(`${environment.apiUrl}/share-password/` + id, ids, this.httpOptions);
  }

  removeUser(password_id: number, user_id: number) {
    return this.http.post(`${environment.apiUrl}/remove-user/` + password_id, {user_id: user_id}, this.httpOptions);
  }
}
