import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';

import {Password} from '../../_models';
import {AlertService, PasswordService} from '../../_services';

@Component({
  selector: 'app-password-dialog',
  templateUrl: 'password-dialog.component.html',
  styleUrls: ['password-dialog.component.css'],
})
export class PasswordDialogComponent implements OnInit {
  title: string;
  edit: boolean;
  passwordForm: FormGroup;
  showPassword: boolean;
  submitted = false;

  constructor(
    public dialogRef: MatDialogRef<PasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public obj: Password,
    private formBuilder: FormBuilder,
    private passwordService: PasswordService,
    private alertService: AlertService) {
    this.title = this.obj === null ? 'New Password' : 'Edit Password';
    this.edit = this.obj === null ? false : true;
    this.showPassword = false;
  }

  /** Convenience getter for easy access to form fields */
  get f() {
    return this.passwordForm.controls;
  }

  ngOnInit() {
    // Set validation rules
    this.passwordForm = this.formBuilder.group({
      project_name: ['', Validators.required],
      project_link: ['', Validators.required],
      description: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // Populate form
    if (this.obj !== null) {
      this.passwordForm.setValue({
        project_name: this.obj.project_name,
        project_link: this.obj.project_link,
        description: this.obj.description,
        username: this.obj.username,
        password: this.obj.password,
      });
    }
  }

  onSubmit() {
    this.submitted = true;

    // Stop here if form is invalid
    if (this.passwordForm.invalid) {
      return;
    }

    // Encrypt password
    const form = this.passwordForm.getRawValue();
    form.password = CryptoJS.AES.encrypt(this.passwordForm.getRawValue().password, 'mwc123').toString();

    if (!this.edit) {
      this.passwordService.store(form)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.alertService.success(data.message, false, 2000);
            this.dialogRef.close(data.password);
          },
          error => {
            this.alertService.error(error, false, 2000);
          });
    } else {
      this.passwordService.update(this.obj.id, form)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.alertService.success(data.message, false, 2000);
            this.dialogRef.close(data.password);
          },
          error => {
            this.alertService.error(error, false, 2000);
          });
    }
  }

  generate(): void {
    this.passwordForm.controls['password'].setValue(this.passwordService.generate(8, true, true, true));
  }

  close(): void {
    this.dialogRef.close();
  }
}
