import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {first} from 'rxjs/operators';
import {AlertService, PasswordService} from '../../_services';

@Component({
  selector: 'app-password-delete-dialog',
  templateUrl: 'password-delete-dialog.component.html',
  styleUrls: ['password-delete-dialog.component.css'],
})
export class PasswordDeleteDialogComponent implements OnInit {
  title: string;
  multiple: boolean;

  constructor(
    public dialogRef: MatDialogRef<PasswordDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public obj: any,
    private passwordService: PasswordService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.title = this.obj instanceof Array ? 'Delete selected' : 'Delete "' + this.obj.project_name + '"';
    this.multiple = this.obj instanceof Array ? true : false;
  }

  onSubmit() {
    if (!this.multiple) {
      this.passwordService.destroy(this.obj.id)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.alertService.success(data.message, false, 2000);
            this.dialogRef.close(data.password);
          },
          error => {
            this.alertService.error(error, false, 2000);
          });
    } else {
      for (let selected of this.obj) {
        this.passwordService.destroy(selected.id)
          .pipe(first())
          .subscribe(
            (data: any) => {
            },
            error => {
              this.alertService.error(error, false, 2000);
            });
      }

      this.alertService.success('All selected passwords have been deleted!', false, 2000);
      this.dialogRef.close(this.obj);
    }
  }

  close(): void {
    this.dialogRef.close();
  }
}
