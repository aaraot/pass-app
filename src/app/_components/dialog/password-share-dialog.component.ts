import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {animate, style, transition, trigger} from '@angular/animations';
import {first} from 'rxjs/operators';
import {AlertService} from '../../_services';
import {UserAPIService} from '../../_services/users-api.service';
import {Password, User} from '../../_models';

@Component({
  selector: 'app-password-share-dialog',
  templateUrl: 'password-share-dialog.component.html',
  styleUrls: ['password-share-dialog.component.css'],
  animations: [
    trigger(
      'sharedWithAnimation', [
        transition(':enter', [
          style({transform: 'translateY(-100%)'}),
          animate('350ms', style({transform: 'translateY(0)'}))
        ])
      ]
    ),
    trigger(
      'sharedWithListAnimation', [
        transition(':enter', [
          style({maxHeight: '0'}),
          animate('350ms', style({maxHeight: '500px'}))
        ])
      ]
    )
  ]
})
export class PasswordShareDialogComponent implements OnInit {
  title: string;
  users: any;
  shared: any;
  selectedUsers: string[];
  sharedList = false;

  constructor(
    public dialogRef: MatDialogRef<PasswordShareDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public obj: Password,
    private userAPIService: UserAPIService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.title = 'Share "' + this.obj.project_name + '"';

    this.userAPIService.getAll(this.obj.id).pipe(first()).subscribe(data => {
      this.users = data.users;
      this.shared = data.shared;
    });
  }

  onSubmit(): void {
    this.userAPIService.sharePassword(this.obj.id, {users: this.selectedUsers})
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.alertService.success(data.message, false, 2000);
          this.dialogRef.close();
        },
        error => {
          this.alertService.error(error, false, 2000);
        });
  }

  removeUser(shared: User): void {
    this.userAPIService.removeUser(this.obj.id, shared.id)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.alertService.success(data.message, false, 2000);
          this.shared.splice(this.shared.indexOf(shared), 1);
          this.users = [...this.users, data.shared];
        },
        error => {
          this.alertService.error(error, false, 2000);
        });
  }

  close(): void {
    this.dialogRef.close();
  }
}
