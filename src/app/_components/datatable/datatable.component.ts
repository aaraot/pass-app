import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatBottomSheet, MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import * as CryptoJS from 'crypto-js';

import {Password} from '../../_models';
import {PasswordDeleteDialogComponent, PasswordDialogComponent, PasswordShareDialogComponent} from '..';

export interface PasswordElement {
  id: number;
  project_name: string;
  project_link: string;
  description: string;
  username: string;
  password: string;
}

@Component({
  selector: 'app-datatable',
  templateUrl: 'datatable.component.html',
  styleUrls: ['datatable.component.css']
})
export class DatatableComponent implements OnInit {
  @Input() columns: Array<any>;
  @Input() data: Array<Password>;

  dataSource: MatTableDataSource<Password>;
  selection = new SelectionModel<PasswordElement>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, private bottomSheet: MatBottomSheet) {
  }

  ngOnInit() {
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  deleteSelection(): void {
    const dialogRef = this.dialog.open(PasswordDeleteDialogComponent, {
      width: '500px',
      data: this.selection.selected
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        for (let selected of result) {
          this.data.splice(this.data.indexOf(selected), 1);
        }

        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection.clear();
      }
    });
  }

  openNewDialog(password: Password): void {
    const dialogRef = this.dialog.open(PasswordDialogComponent, {
      width: '500px',
      data: password
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (password) {
          Object.assign(this.data[this.data.indexOf(password)], result);
        } else {
          this.data.push(result);
        }

        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openDeleteDialog(password: Password): void {
    const dialogRef = this.dialog.open(PasswordDeleteDialogComponent, {
      width: '500px',
      data: password
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.data.splice(this.data.indexOf(password), 1);
        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  showPassword(password: Password): void {
    if (typeof password.password !== 'object') {
      password.password = {
        show: true,
        password: CryptoJS.AES.decrypt(password.password, 'mwc123').toString(CryptoJS.enc.Utf8)
      };
    } else {
      password.password.show = !password.password.show;
    }
  }

  sharePassword(password: Password): void {
    const dialogRef = this.dialog.open(PasswordShareDialogComponent, {
      width: '500px',
      data: password
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('asd');
      }
    });
  }
}
